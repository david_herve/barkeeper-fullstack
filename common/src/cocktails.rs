use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Cocktail {
    pub id: uuid::Uuid,
    pub name: String,
    pub rating: u8,
}
