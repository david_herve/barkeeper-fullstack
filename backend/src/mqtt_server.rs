use actix::prelude::*;
use paho_mqtt as mqtt;
use std::{env, process, thread, time::Duration};

// Define settings
const DFLT_BROKER: &str = "tcp://broker.emqx.io:1883";
const DFLT_CLIENT: &str = "rust_subscribe";
const DFLT_TOPICS: &[&str] = &["mqtt/temperature", "mqtt/frequency", "mqtt/humidity"];

// The qos list that match topics above.
const DFLT_QOS: &[i32] = &[0, 1, 2];

/// Reconnect to the broker when connection is lost.
fn try_reconnect(cli: &mqtt::Client) -> bool {
    println!("Connection lost. Waiting to retry connection");
    for _ in 0..12 {
        thread::sleep(Duration::from_millis(5000));
        if cli.reconnect().is_ok() {
            println!("✅ Successfully reconnected");
            return true;
        }
    }
    println!("Unable to reconnect after several attempts.");
    false
}
// Subscribes to multiple topics.
fn subscribe_topics(cli: &mqtt::Client) {
    if let Err(e) = cli.subscribe_many(DFLT_TOPICS, DFLT_QOS) {
        println!("Error subscribes topics: {:?}", e);
        process::exit(1);
    }
}
pub struct Mqtt;

impl Mqtt {
    pub fn new() -> Self {
        Mqtt
    }
}

impl Actor for Mqtt {
    type Context = Context<Self>;

    fn started(&mut self, _ctx: &mut Self::Context) {
        // Define host
        let host = env::args()
            .nth(1)
            .unwrap_or_else(|| DFLT_BROKER.to_string());

        // Define the set of options for the create.
        // Use an ID for a persistent session.
        let create_opts = mqtt::CreateOptionsBuilder::new()
            .server_uri(host)
            .client_id(DFLT_CLIENT.to_string())
            .finalize();

        // Create a client.
        let cli = mqtt::Client::new(create_opts).unwrap_or_else(|err| {
            panic!("Error creating the client: {:?}", err);
        });

        // Initialize the consumer before connecting.
        let rx = cli.start_consuming();

        // Define the set of options for the connection.
        let lwt = mqtt::MessageBuilder::new()
            .topic("test")
            .payload("Consumer lost connection")
            .finalize();
        let conn_opts = mqtt::ConnectOptionsBuilder::new()
            .keep_alive_interval(Duration::from_secs(20))
            .clean_session(false)
            .will_message(lwt)
            .finalize();

        // Connect and wait for it to complete or fail.
        if let Err(e) = cli.connect(conn_opts) {
            println!("Unable to connect:\n\t{:?}", e);
            process::exit(1);
        }

        // Subscribe topics.
        subscribe_topics(&cli);

        // Start event loop in thread
        thread::spawn(move || {
            println!("✅ Connection to the mqtt broker is successful!");

            // Iterate messages
            for msg in rx.iter() {
                if let Some(msg) = msg {
                    println!("{}", msg);
                } else if !cli.is_connected() {
                    if try_reconnect(&cli) {
                        println!("Resubscribe topics...");
                        subscribe_topics(&cli);
                    } else {
                        break;
                    }
                }
            }
        });
    }

    fn stopped(&mut self, _ctx: &mut Self::Context) {
        // If still connected, then disconnect now.
        // if self.is_connected() {
        //     println!("Disconnecting");
        //     self.cli.unsubscribe_many(DFLT_TOPICS).unwrap();
        //     self.cli.disconnect(None).unwrap();
        // }
        // println!("Exiting");
    }
}
