use actix_web::web;


pub mod cocktails_handler;
pub mod cocktails_model;
pub mod cocktails_schema;

pub fn config(conf: &mut web::ServiceConfig) {
    // add cocktails handler
    let scope = web::scope("/api/v1")
        .service(cocktails_handler::cocktails_list_handler)
        .service(cocktails_handler::create_cocktail_handler);

    conf.service(scope);
}
