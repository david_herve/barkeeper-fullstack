use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize)]
pub struct FilterOptions {
    pub page: Option<usize>,
    pub limit: Option<usize>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct CreateCocktailSchema {
    pub name: String,
    pub rating: i32,
}
