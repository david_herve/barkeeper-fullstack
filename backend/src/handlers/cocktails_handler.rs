use super::cocktails_model::*;
use super::cocktails_schema::*;
use crate::AppState;
use actix_web::{get, post, web, HttpResponse, Responder};
use serde_json::json;

#[get("/cocktails")]
pub async fn cocktails_list_handler(
    opts: web::Query<FilterOptions>,
    data: web::Data<AppState>,
) -> impl Responder {
    // Get limit with parameters
    let limit = opts.limit.unwrap_or(10);
    let offset = (opts.page.unwrap_or(1) - 1) * limit;

    // Make query on db
    let query_result = sqlx::query_as!(
        CocktailModel,
        "SELECT * from cocktails ORDER by name LIMIT $1 OFFSET $2",
        limit as i32,
        offset as i32,
    )
    .fetch_all(&data.db)
    .await;

    // Match result for returning a json
    match query_result {
        Err(err) => {
            let message = format!(
                "Something bad happened while fetching all cocktail items: {}",
                err
            );
            HttpResponse::InternalServerError().json(json!({
                "status": "error",
                "message": message,
            }))
        }
        Ok(cocktails) => {
            let json_reponse = serde_json::json!({
                "status": "success",
                "result": cocktails.len(),
                "cocktails": cocktails
            });
            HttpResponse::Ok().json(json_reponse)
        }
    }
}

#[post("/cocktails")]
pub async fn create_cocktail_handler(
    body: web::Json<CreateCocktailSchema>,
    data: web::Data<AppState>,
) -> impl Responder {
    // Create a future query
    let query_result = sqlx::query_as!(
        CocktailModel,
        "INSERT INTO cocktails (name, rating) VALUES ($1, $2) RETURNING *",
        body.name.to_string(),
        body.rating,
    )
    .fetch_one(&data.db)
    .await;

    // Run and match query
    match query_result {
        Ok(cocktail) => {
            let feedback_response = serde_json::json!({"status": "success", "data": serde_json::json!({"cocktail": cocktail})});
            HttpResponse::Ok().json(feedback_response)
        }
        Err(err) => {
            if err
                .to_string()
                .contains("duplicate key value violates unique constraint")
            {
                HttpResponse::BadRequest().json(serde_json::json!({
                        "status": "fail",
                        "message": "Cocktail with that name already exists"
                }))
            } else {
                HttpResponse::InternalServerError().json(serde_json::json!({
                    "status": "error",
                    "message": format!("{:?}", err)
                }))
            }
        }
    }
}
