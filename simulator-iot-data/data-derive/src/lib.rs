use core::panic;

use proc_macro::TokenStream;
use quote::quote;
use rand::Rng;
use syn::{DeriveInput, Ident};

fn impl_data_trait(ast: DeriveInput) -> TokenStream {
    // get struct identifier
    let ident = ast.ident;
    let ident_str = ident.to_string().to_lowercase();

    // get fields identifier
    let field_idents: Vec<Ident> = match ast.data {
        syn::Data::Struct(data) => data.fields.into_iter().filter_map(|f| f.ident).collect(),
        syn::Data::Enum(_) => panic!("Enums are not supported by data trait"),
        syn::Data::Union(_) => panic!("Unions are not supported by data trait"),
    };

    let field_idents_str: Vec<String> = field_idents.iter().map(|f| f.to_string()).collect();

    // Check field
    if field_idents_str
        .iter()
        .find(|f| f.eq_ignore_ascii_case("received_at"))
        .is_none()
    {
        panic!("Need to have a \"received_at\" field");
    }

    // Check field
    if field_idents_str
        .iter()
        .find(|f| f.eq_ignore_ascii_case("value"))
        .is_none()
    {
        panic!("Need to have a \"value\" field");
    }

    // generate impl
    quote! {
        impl Data for #ident {
            fn now() -> #ident {
                #ident {
                    received_at: Utc::now().to_rfc3339(),
                    value: rand::thread_rng().gen(),
                    ..std::default::Default::default()
                }
            }
            fn topic() -> String {
                format!("mqtt/{}", #ident_str).into()
            }
            fn to_str(&self) -> String {
                match serde_json::to_string(&self) {
                    Ok(json) => json,
                    Err(err) => panic!("Error when parse to json {}: {}", #ident_str, err)
                }
            }
        }
    }
    .into()
}

#[proc_macro_derive(Data)]
pub fn data_derive_macro(item: TokenStream) -> TokenStream {
    // parse
    let ast = syn::parse(item).unwrap();

    // generate
    impl_data_trait(ast)
}
