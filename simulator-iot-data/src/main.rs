use chrono::prelude::*;
use crossbeam_channel::{bounded, select, tick, Receiver};
use data_derive::Data;
use paho_mqtt as mqtt;
use rand::Rng;
use serde::Serialize;
use std::{env, process, time::Duration};

// Define config
const DFLT_BROKER: &str = "tcp://broker.emqx.io:1883";
const DFLT_CLIENT: &str = "rust_publish";
const DFLT_TOPICS: &[&str] = &[
    "mqtt/temperature",
    "mqtt/frequency",
    "mqtt/humidity",
    "mqtt/wind",
    "mqtt/flowrate",
    "mqtt/weight",
    "mqtt/position",
];

// Define the qos.
const QOS: i32 = 1;

// Define Ctrc-C
fn ctrl_channel() -> anyhow::Result<Receiver<()>, ctrlc::Error> {
    let (sender, receiver) = bounded(100);
    ctrlc::set_handler(move || {
        let _ = sender.send(());
    })?;

    Ok(receiver)
}

trait Data: Default + Serialize {
    fn now() -> Self;
    fn topic() -> String;
    fn to_str(&self) -> String;
}

#[derive(Serialize, Default, Data)]
struct Temperature {
    value: f32,
    received_at: String,
}

#[derive(Serialize, Default, Data)]
struct Weight {
    value: f32,
    received_at: String,
}

#[derive(Serialize, Default, Data)]
struct Frequency {
    value: f32,
    received_at: String,
}

fn send_data<D: Data>(cli: &mqtt::Client) {
    // Create a temperature message and publish it.
    let msg = mqtt::Message::new(D::topic(), D::now().to_str(), QOS);
    match cli.publish(msg) {
        Ok(_) => println!("Publishing messages on the {:?} topic", D::topic()),
        Err(e) => println!("Error sending message: {:?}", e),
    };
}

fn main() -> anyhow::Result<()> {
    // Define the host
    let host = env::args()
        .nth(1)
        .unwrap_or_else(|| DFLT_BROKER.to_string());

    // Define the set of options for the create.
    // Use an ID for a persistent session.
    let create_opts = mqtt::CreateOptionsBuilder::new()
        .server_uri(host)
        .client_id(DFLT_CLIENT.to_string())
        .finalize();

    // Create a client.
    let cli = mqtt::Client::new(create_opts).unwrap_or_else(|err| {
        println!("Error creating the client: {:?}", err);
        process::exit(1);
    });

    // Define the set of options for the connection.
    let conn_opts = mqtt::ConnectOptionsBuilder::new()
        .keep_alive_interval(Duration::from_secs(20))
        .clean_session(true)
        .finalize();

    // Connect and wait for it to complete or fail.
    if let Err(e) = cli.connect(conn_opts) {
        println!("Unable to connect:\n\t{:?}", e);
        process::exit(1);
    }

    // Create a handler for stop loop.
    let ctrl_c_events = ctrl_channel()?;
    let ticks = tick(Duration::from_secs(1));
    loop {
        select! {
            recv(ticks) -> _ => {

                send_data::<Temperature>(&cli);
                send_data::<Weight>(&cli);
                send_data::<Frequency>(&cli);

            }
            recv(ctrl_c_events) -> _ => {
                // Disconnect from the broker.
                let tok = cli.disconnect(None);
                println!("Disconnect from the broker");
                tok.unwrap();
                break;
            }
        }
    }

    Ok(())
}
